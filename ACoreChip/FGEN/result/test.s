.global _start
_start:
li t0, 0x20000004

####################FADD####################

li t1, 0x7f800000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fadd.s ft2, ft0, ft1

####################FSUB####################

li t1, 0x7f800000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsub.s ft2, ft0, ft1

####################FMUL####################

li t1, 0x7f800000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fmul.s ft2, ft0, ft1

####################FDIV####################

li t1, 0x7f800000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fdiv.s ft2, ft0, ft1

####################FSQRT####################

li t1, 0x7f800000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7f800000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0xff800000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000000
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fbfffff
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x7fffffff
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00800001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80800001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x00000001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x7f800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0xff800000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x00000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x80000000
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x7fbfffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x7fffffff
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x00800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x80800001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x00000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

li t1, 0x80000001
li t2, 0x80000001
sw t1, 0(t0)
flw ft0, 0(t0)
sw t2, 4(t0)
flw ft1, 4(t0)
fsqrt.s ft2, ft0, ft1

