"""
========
FSQRT
========

FSQRT algorithm test code

"""

import os
import sys
import numpy as np

class FSQRT():

    def __init__(self,*arg): 
        """ FSQRT parameters and attributes
        """
        self.k0 = 4
        self.iterations = 25
        self.inv_An = self.calc_An()
        print(self.inv_An)

    def calc_An(self):
        subvalues = []
        k = self.k0 # Used for the repeated (3*k + 1) iteration steps
        for i in range(1, self.iterations+1):
            subvalues.append(np.sqrt(1-2**(-2*i)))
            if (i == k):
                subvalues.append(np.sqrt(1-2**(-2*i)))
                k = 3 * k + 1
        return 1/np.cumprod(subvalues)[-1]

    def cordic_sqrt_mant(self, mantissa):
        ''' Perform cordic squareroot for 24bit mantissa
        '''

        k = self.k0 # Used for the repeated (3*k + 1) iteration steps

        x = mantissa + 0.25 # calculate x0
        y = mantissa - 0.25 # calculate y0
        print("x: ", x, "\ny: ", y)
        for i in range(1, self.iterations+1):
            # Multiply by 2^(-i)
            xtmp = x * 2**(-i)
            ytmp = y * 2**(-i)

            if (y < 0):
                x += ytmp
                y += xtmp
            else:
                x -= ytmp
                y -= xtmp

            if (i == k):
                # Multiply by 2^(-i)
                xtmp = x * 2**(-i)
                ytmp = y * 2**(-i)

                if (y < 0):
                    x += ytmp
                    y += xtmp
                else:
                    x -= ytmp
                    y -= xtmp

                    k = 3 * k + 1
            print("x: ", x, "\ny: ", y)
        return x*self.inv_An #Cordic gain
 


if __name__=="__main__":

    ExpA = (146 - 128) / 2
    ExpB = (143 - 127) / 2
    ExpC = (144 - 128) / 2

    mantA = 1.6947858333587646*2**(-1)
    mantB = 1.8787097930908203
    mantC = 1.8599458932876587*2**(-1)

    mantY = 1.8787097930908203
   
    ent = FSQRT()

    resultA = ent.cordic_sqrt_mant(mantA)

    print("resA:  ",2**(ExpA) * (resultA*2)) #1.8410789966583252

    resultB = ent.cordic_sqrt_mant(mantB)

    print("resB:  ",2**(ExpB) * (resultB)) #1.8410789966583252

    #resultC = ent.cordic_sqrt_mant(mantC)

    #print("resC:  ",2**(ExpC) * (resultC*2)) #1.8410789966583252

    resultY = ent.cordic_sqrt_mant(mantY)

    input()
