#!/usr/bin/env python3

# GTKWave Translate Process File to decode hex value into RISC-V operation
# Credits go to Matt Venn
# Source: https://github.com/mattvenn/gtkwave-python-filter-process (2022)

import sys
import tempfile
import subprocess


def main():
    fh_in = sys.stdin
    fh_out = sys.stdout
    instr_dict = {}

    while True:
        l = fh_in.readline()
        if not l:
            return 0

        if "x" in l:
            fh_out.write(l)
            fh_out.flush()
            continue
        
        if str(l) not in instr_dict.keys():
            obj_temp = tempfile.NamedTemporaryFile(delete=False, mode='w')
            with tempfile.NamedTemporaryFile(delete=False, mode='w') as asm_temp:
                asm_temp.write(".word 0x%s\n" % l)
                asm_temp.flush()
                subprocess.run(["riscv64-unknown-elf-as", "-march=rv32imfc", "-o", obj_temp.name, asm_temp.name])
                result = subprocess.run(["riscv64-unknown-elf-objdump", "-Dj", ".text", obj_temp.name], stdout=subprocess.PIPE)
                lastline = result.stdout.splitlines()[-1]
                chunks = lastline.decode().split('\t')

                opcodes = " ".join(chunks[2:])
                instr_dict[str(l)] = opcodes
            
        fh_out.write("%s\n" % instr_dict[str(l)])
        fh_out.flush()


if __name__ == '__main__':
	sys.exit(main())
